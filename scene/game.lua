-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here

local composer = require("composer")

local scene = composer.newScene()

function scene:create( event )
	
	local count = 0

	local message = "Clique no lugar para onde você quer se mover"

	local background = display.newImageRect( "background.jpg", 1900, 1050 )
	background.x = display.contentCenterX
	background.y = display.contentCenterY

	local detective = display.newImageRect( "detective1.png", 100, 140 )
	detective.x = display.contentWidth-1200
	detective.y = display.contentHeight-400

	local button = display.newImageRect( "button.png", 140, 150 )
	button.x = display.contentWidth- -400
	button.y = display.contentHeight-100

	local physics = require( "physics" )
	physics.start()

	physics.addBody( button, "static")
	physics.addBody( detective, "kinematic" )

	local function makeTheDetectiveWalk(event)
		if(event.phase == "ended" and count > 1) then
			transition.to(detective, {x=event.x, y=event.y})
		end
		count = count + 1 
	end

	Runtime:addEventListener("touch", makeTheDetectiveWalk)	

	local function showHint()
		if(count <= 2) then
			local tapText = display.newText( message, display.contentCenterX, 50, native.systemFont, 60 )
		end
	end

	button:addEventListener( "tap", showHint )
end

scene:addEventListener( "create" )
-- scene:addEventListener( "show" )
-- scene:addEventListener( "hide" )
-- scene:addEventListener( "destroy" )

return scene